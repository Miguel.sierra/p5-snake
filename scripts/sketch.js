var culebra;
var comida;
var auxX;
var auxY;
var puntaje = 0;
var muerte = false;
function setup() {
  createCanvas(630, 400);
  frameRate(8);
  culebra = new culebrita();
  comida = new comida();
  $("#score").html(0);

}


function draw() {
  background(40);
  culebra.update();
  culebra.edges();
  culebra.show();
  comida.show();
  if (culebra.comer(comida)) {
    comida.mover();
    puntaje++;
    console.log(puntaje);
    $("#score").html(puntaje);
    
  }

  if (culebra.morir()) {
    muerte = true;
    $("#mensaje_muerte").html("PERDISTE");
    culebra.xspeed = 0;
    culebra.yspeed = 0;
    
  }


}


function keyPressed(){
  if (!muerte) {
    if (keyCode == UP_ARROW  && culebra.yspeed <= 0) {
      culebra.dir(0,Math.round10(-10,1));    
    }else if(keyCode == DOWN_ARROW && culebra.yspeed >= 0){
      culebra.dir(0,Math.round10(10,1));
    }else if (keyCode == LEFT_ARROW && culebra.xspeed <= 0) {
      culebra.dir(Math.round10(-10,1),0);
    }else if (keyCode == RIGHT_ARROW && culebra.xspeed >= 0) {
      culebra.dir(Math.round10(10,1),0);
    }else if (keyCode == 32) {
      //Pausa el juego al presionar espacio
      if (culebra.xspeed != 0 || culebra.yspeed != 0) {
        auxX = culebra.xspeed;
        auxY = culebra.yspeed;
        culebra.cola.xspeed = 0;
        culebra.cola.yspeed = 0;
        culebra.xspeed = 0; 
        culebra.yspeed = 0;

      }else{
        culebra.xspeed = auxX;
        culebra.yspeed = auxY;
        culebra.cola.xspeed = auxX;
        culebra.cola.yspeed = auxY;
      }
  
    }
  }
}





