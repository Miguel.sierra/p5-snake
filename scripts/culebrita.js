

function culebrita(){
    this.x = 0;
    this.y = 0;
    this.r = 10;
    this.xspeed = 10;
    this.yspeed = 0;
    this.cola = [];
    
  
    this.dir = function(posx, posy){
      this.xspeed = posx;
      this.yspeed = posy;
    }
  
    this.comer = function(comida) {
  
      var d = dist(this.x, this.y, comida.x, comida.y);
      //console.log("Posiciones de la serpiente:   X: "+this.x+"  Y: "+this.y);  
      //console.log("Posiciones de la comida:   X: "+comida.x+"  Y: "+comida.y);  
      if (d <= 1) {
        return true;
      } else{
        return false;
      }
    }
  
    //Para que no se vaya
    this.edges = function() {
      if (this.x > width+this.r) {
        this.x =  Math.round10(-this.r,1);
      }else if (this.x < -this.r) {
        this.x =  Math.round10(width + this.r,1);
      }else if (this.y > height + this.r) {
        this.y =  Math.round10(-this.r,1);
      }else if (this.y < -this.r) {
        this.y =  Math.round10(height + this.r,1);
      }
    }
  
    this.morir = function(){
      for (let index = 0; index < this.cola.length; index++) {
        var pos = this.cola[index];
        var d = dist(this.x,this.y,pos.x,pos.y);
        if (d < 1) {
          return true;
        }else{
          return false;
        }
        
      }
    }
  
  
    this.update = function(){
      if (puntaje === this.cola.length) {
        for (let i = 0; i < this.cola.length-1; i++) {
          this.cola[i] = this.cola[i+1];      
        }
      }
      this.cola[puntaje-1] =  createVector(this.x, this.y);
  
  
      this.x = this.x + this.xspeed;
      this.y = this.y + this.yspeed;
    }
  
  
    this.show = function(){
      fill(255);
      for (let index = 0; index < puntaje; index++) {
        rect(this.cola[index].x, this.cola[index].y, this.r,this.r);      
      }
      //Para que aumente de tama�o el primer this.r debe ser multiplicado x2 pero la cabeza debe ser la �nica
      //que se mueva
      rect(this.x,this.y,this.r,this.r);
    }
  }